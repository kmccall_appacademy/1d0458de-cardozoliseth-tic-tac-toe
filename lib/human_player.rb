class HumanPlayer
  attr_reader :name
  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where do you like to move, enter: row, column"
    mark = gets.chomp
    [mark[0].to_i, mark[-1].to_i]
  end

  def display(board)
    row_0 = "0 |"
    (0..2).each do |column|
      row_0 << (board.empty?([0, column]) ? "   |" : " " + board[[0, column]].to_s + " |")
    end
    row_1 = "1 |"
    (0..2).each do |column|
      row_1 << (board.empty?([1, column]) ? "   |" : " " + board[[1, column]].to_s + " |")
    end
    row_2 = "2 |"
    (0..2).each do |column|
      row_2 << (board.empty?([2, column]) ? "   |" : " " + board[[2, column]].to_s + " |")
    end

    puts "    0   1   2  "
    puts "  |-----------|"
    puts row_0
    puts "  |-----------|"
    puts row_1
    puts "  |-----------|"
    puts row_2
    puts "  |-----------|"
  end

end
