class Board
  attr_reader :grid

  def self.start_grid
    Array.new(3) { Array.new(3)}
  end

  def initialize(grid = Board.start_grid)
    @grid = grid
  end

  def place_mark(position, sym)
    self[position] = sym
  end

  def [](position)
    row, column = position
    @grid[row][column]
  end

  def []=(position, mark)
    row, column = position
    @grid[row][column] = mark
  end

  def empty?(position)
    self[position].nil?

  end

  def winner
    (grid + columns + diagonals).each do |triple|
      return :X if triple == [:X, :X, :X]
      return :O if triple == [:O, :O, :O]
    end
    nil
  end

  def diagonals
    diagonal_down =[[0,0],[1,1],[2,2]]
    diagonal_up = [[0,2],[1,1],[2,0]]
    [diagonal_down, diagonal_up].map do |diagonal|
      diagonal.map { |row, column| grid[row][column]}
    end
  end

  def columns
    columns = [[],[],[]]
    grid.each do |row|
      row.each_with_index do | mark, column_idx|
        columns[column_idx] << mark
      end
    end
    columns
  end

  def over?
    grid.flatten.none? { |pos| pos.nil? } || winner
  end

end
